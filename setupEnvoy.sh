echo "Running this on $HOSTNAME"
echo "Setting default timezone (Asia/Kolkata +0530)"
sudo unlink /etc/localtime
sudo ln -s /usr/share/zoneinfo/Asia/Kolkata /etc/localtime

echo "Minimal set of packages to be initially installed"
sudo apt-get update
sudo apt-get -y install php-cli php-fpm php-curl php-gd php-zip php-mbstring php-soap php-mysql php-xml git zip unzip composer curl jq python-pip supervisor git-crypt mysql-client-core-5.7
sudo service supervisor restart

echo "Checking if Envoy is installed..."
type envoy 1>/dev/null 2>/dev/null
exists=$?
if [ $exists -eq 1 ];
then
	echo "Installing Laravel Envoy"
	sudo composer global require "laravel/envoy"
	sudo ln -sf ~/.composer/vendor/bin/envoy /usr/local/bin/envoy
	echo "Laravel Envoy is ready. Use it using \"envoy\" command."
else
	echo "Envoy is already installed."
fi

echo "Change shell to bash as required by Envoy"
sudo ln -sf /bin/bash /bin/sh