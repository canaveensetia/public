#!/usr/bin/env bash

usage() { echo "Usage: $0 -t <Github Token>" 1>&2; exit 1; }
status=`ssh -T -o StrictHostKeyChecking=no git@github.com 2>&1`
if [[ $status == *"successfully authenticated"* ]]
then
        echo "Github authorization is already set up. Continuing."
else
    keyType="ssh"
    export url="https://api.github.com/user/naveensetia/keys"
    export createUrl="https://api.github.com/user/keys"

    while getopts ":t:" o; do
        case "${o}" in
            t)
                gitToken=${OPTARG}
                ;;
            *)
                usage
                ;;
        esac
    done

    if [ "$gitToken" = "" ];
    then
        echo "Github Token is mandatory. Please enter the token below." 1>&2
        read -sp 'Github Token: ' gitToken
    fi

    #Install Jq
    op=`type jq 2>&1 || true`
    if [[ $op == *"not found"* ]];
    then
        echo "Installing dependency \"jq\"" 1>&2
        sudo add-apt-repository universe
        sudo apt-get update
        sudo apt-get -y install jq
    else
        echo "Jq is already installed. Skipping it."
    fi

    #Install Curl
    op=`type curl 2>&1 || true`
    if [[ $op == *"not found"* ]];
 then
        echo "Installing dependency \"Curl\"" 1>&2
        sudo apt-get -y install curl
    else
        echo "Curl is already installed. Skipping it."
    fi

    sshUser=$(whoami)
    echo "SSH User: $sshUser"
    i=1
    #echo "Getting MAC address..."
    mac=`cat /sys/class/net/$(ip route show default | awk '/default/{print $5}')/address`

        if [ -z "$mac" ]
        then
                #This adjustment has been done for windows sub shell as it add one extra word via so position gets shifted by one word
                mac=`cat /sys/class/net/$(ip route show default | awk '/default/{print $6}')/address`
        fi

    keyLabel="github"_"$HOSTNAME"_"$mac"
    keyFileName="$keyLabel"

    #echo "Testing if Github authorization has already been setup..."
        echo "Authorization failed. Setting up access."
        ssh-keygen -R github.com
        curl --silent --header "PRIVATE-TOKEN: $gitToken" $url > .keys
        len=$(cat .keys | jq ". | length")
        echo $len
        if [[ -z $len && $len -gt 0 ]]
        then
                len=$len
        else
                len=0
        fi

        while [ $len -gt 0 ];
        do
                i=$(($len-1))
                label=$(cat .keys | jq ".[$i].title")
                label="${label%\"}"
                label="${label#\"}"
               if [ "$label" = "$keyLabel" ];
                then
                        id=$(cat .keys | jq ".[$i].id")
                        curl --silent --header "PRIVATE-TOKEN: $gitToken" -X "DELETE" $url/$id > /dev/null
                        echo "Deleted existing keys with name $keyLabel"
                fi
                len=$(( $len - 1 ))
        done

        echo -e "y\n" | ssh-keygen -t rsa -q -b 4096 -f /home/$sshUser/.ssh/$keyFileName -N "" -C ""

        #Add the key to ssh-agent
        ssh-add "/home/$sshUser/.ssh/$keyFileName"
        eval `ssh-agent -s`

        #Upload the key to github.com
        public_key="/home/$sshUser/.ssh/$keyFileName.pub"
        postBody="{\"title\":\"$keyLabel\", \"key\":\"$(cat $public_key)\"}"

        result=$(curl -o /dev/null --user "naveensetia:$gitToken" -w "%{http_code}" --silent --header "PRIVATE-TOKEN: $gitToken" -H "Content-Type: application/json" -X "POST" -d "$(echo -e $postBody)" $createUrl)

        if [ $result -eq 201 ];
        then
                echo "Status $result: Successfully uploaded new $keyType key $keyLabel."
                if [ -f ~/.ssh/config ];
                then
                        count=$(cat ~/.ssh/config | grep -n "github.com"| wc -l )
                else
                        count=0
                fi
                if [ "$count" -gt 0 ];
                then
                        echo -e "Note: $count configuration(s) for github.com already exist(s) in ~/.ssh/config." 1>&2
                else
                        echo -e "Host github.com\nIdentityFile /home/$sshUser/.ssh/$keyFileName\nStrictHostKeyChecking no" |tee -a ~/.ssh/config
        fi
        else
                echo "Status $result: An error occurred while uploading deployment key." 1>&2
        fi
fi
rm -f ./.keys
